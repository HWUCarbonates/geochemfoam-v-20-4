#!/bin/bash




# ###### DO NOT MAKE CHANGES FROM HERE ###################################



set -e


python << END
import os
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
a=0;
s=str(0)
for directories in os.listdir(os.getcwd()): 
  if (is_number(directories)):
    if (float(directories)>a):
      a=float(directories)
      s=directories
os.system('rm -rf '+s+'/polyMesh/sets/*')
os.system('topoSet')
os.system('createPatch -overwrite')
END


