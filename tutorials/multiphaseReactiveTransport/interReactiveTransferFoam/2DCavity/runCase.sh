#!/bin/bash

set -e

decomposePar
mpiexec -np 8 interReactiveTransferFoam -parallel
reconstructPar
rm -rf processor*
