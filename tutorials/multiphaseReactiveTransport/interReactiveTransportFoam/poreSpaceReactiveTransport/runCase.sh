#!/bin/bash

set -e

decomposePar
mpiexec -np 24 interReactiveTransportFoam -parallel
reconstructPar
rm -rf process*


